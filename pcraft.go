package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
	"strings"
)

func main() {
	var data []byte
	var dataString string
	var verbose bool = false
	var ip string
	var binaryName = os.Args[0]

	if isPipe() {
		/*
			scanner := bufio.NewScanner(os.Stdin)
			for scanner.Scan() {
				data = append(data, scanner.Bytes()...)
			}
			err = scanner.Err()
			er(err)
		*/

		stdin, err := io.ReadAll(os.Stdin)
		er(err)

		dataString = strings.TrimSuffix(string(stdin), "\r\n")

		for i := 0; i < len(dataString); i++ {
			if i+1 < len(dataString) && i%2 == 0 {
				tmp, err := strconv.ParseUint(fmt.Sprintf("%c%c", dataString[i], dataString[i+1]), 16, 8)
				er(err)

				data = append(data, byte(tmp))
			}
		}

	}

	if len(os.Args) == 1 {
		os.Args = append(os.Args, "-h")
	}

	args := os.Args[1:]
	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "--help" || arg == "-h" {
			fmt.Print("-h | --help\tPrint this help message.\n")
			fmt.Print("-v | --verbose\tShow more information.\n")
			fmt.Print("<STDIN>\t\tExpects a hex string for the packet to send (including layer 3, i.e. TCP/UDP/ICMP data;\n")
			fmt.Print("\t\ti.e. Everything inside of the IP encapsulation).\n")
			fmt.Print("A.B.C.D\t\tDestination IP address as arguemnt.\n")
			fmt.Print("\nNote:\tThere will be obvious issues with checksum validation depending upon the protocol used if you are not manually recalculating the checksum.\n")
			fmt.Print("\nExample:\n")
			fmt.Printf("echo \"AB23DD4460...\" | %s 1.2.3.4\n\n", binaryName)

			return
		}

		if arg == "--verbose" || arg == "-v" {
			verbose = true
		}

		regex := regexp.MustCompile(`^\d+\.\d+\.\d+\.\d+$`)
		if regex.MatchString(arg) {
			ip = arg
		}
	}

	if len(ip) == 0 || len(data) == 0 {
		return
	}

	if verbose {
		fmt.Print("Sending data:\n")
		fmt.Printf("%X", data)
		fmt.Printf("\nTo Address: %s\n", ip)
	}

	sendData(data, ip)
}

func sendData(data []byte, ip string) {
	conn, err := net.Dial("ip4:1", ip)
	er(err)
	defer conn.Close()

	_, err = conn.Write(data)
	er(err)
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(),
			path.Base(file), line, err)
	}
}

func isPipe() bool {
	fileInfo, _ := os.Stdin.Stat()
	return fileInfo.Mode()&os.ModeCharDevice == 0
}
